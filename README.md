
# Setup easy restart of web services command
sudo nano ~/.profile

# add the following to the end of the file
alias rsweb="sudo nginx -t && sudo service nginx restart && sudo service php-fpm restart"

# After relogging you can now type the to restart all web shit
rsweb

# update/install NGINX
sudo add-apt-repository ppa:nginx/stable
sudo apt-get update
sudo apt install nginx
sudo apt-get install nginx-extras
sudo apt-get upgrade

#allow web ports through firewall if you have "ufw"
sudo ufw allow 'Nginx HTTP'
sudo ufw allow 'Nginx HTTPS'
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw allow 3000:3999/tcp
sudo ufw allow 3000:3999/udp
sudo ufw allow 8000:9999/tcp
sudo ufw allow 8000:9999/udp

# Install MySQL if you dont have it already
sudo apt install mysql-server phpmyadmin
sudo mysql_secure_installation

# Install PHP for NGINX (php-fpm)
sudo apt install php-fpm php-mysql
sudo phpenmod mcrypt

# Link phpmyadmin to a folder you can access
sudo ln -s /usr/share/phpmyadmin /var/www/html/store/public

# Example server config for PHP

Copy all the files in nginx_conf.zip into /etc/nginx and then move server.conf
to wherever your main server config is

# Test NGINX

sudo nginx -t
sudo systemctl reload nginx
sudo systemctl status nginx
